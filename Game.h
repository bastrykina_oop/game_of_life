#pragma once
#include "Game of life.h"
#include "Cell.h"

class Game {
	Cell prev_field[10][10];
	Cell field[10][10];
	int steps_counter;
	CellState state_predictor(int a, int b);
	void show() const;
	void load(const std::string f_name);
	void random_set();
	void set(int i, int j);
	void erase(int i, int j);
	void reset();
	void step();
	void step(int n);
	void save(const std::string f_name);
public:
	Game();
	void help();
	int exec_command();
};
