﻿#pragma once
enum class CellState {Dead = '.', Alive = 'o'};

class Cell {
	CellState st;
public:
	Cell();
	bool is_alive();
	CellState get_state() const;
	Cell& operator=(const Cell& c);
	void set_state(CellState s);
};