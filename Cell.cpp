#include "Cell.h"

Cell::Cell() {
	st = CellState::Dead;
};

bool Cell::is_alive() {
	return (st == CellState::Alive);
}

CellState Cell::get_state() const {
	return st;
}

Cell& Cell::operator=(const Cell& c) {
	if (this != &c) st = c.st;
	return *this;
}

void Cell::set_state(CellState s) {
	st = s;
}