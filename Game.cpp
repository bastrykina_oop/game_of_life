#include "Game.h"

CellState Game::state_predictor(int a, int b) {
	int living_neigbours = 0;
	int real_i, real_j;
	for (int i = a - 1; i <= a + 1; i++)
		for (int j = b - 1; j <= b + 1; j++) {
			if ((i == a) && (j == b)) continue;
			real_i = i;
			real_j = j;
			if (i == -1) real_i = 9;
			if (j == -1) real_j = 9;
			if (i == 10) real_i = 0;
			if (j == 10) real_j = 0;
			if (prev_field[real_i][real_j].is_alive()) living_neigbours++;
		}
	if (prev_field[a][b].is_alive() && (living_neigbours == 2 || living_neigbours == 3)) return CellState::Alive;
	if (!prev_field[a][b].is_alive() &&  living_neigbours == 3) return CellState::Alive;
	return CellState::Dead;
}

Game::Game() {
	steps_counter = 0;
}

void Game::show() const {
	system("cls");
	std::cout << "Current step: " << steps_counter << std::endl;
	std::cout << "  0  1  2  3  4  5  6  7  8  9 " << std::endl;
	for (int i = 0; i <= 9; i++) {
		std::cout << i;
		for (int j = 0; j <= 9; j++) std::cout << ' ' << char(field[i][j].get_state()) << ' ';
		std::cout << std::endl;
	}
}

void Game::random_set() {
	srand(time(0));
	for (int i = 0; i <= 9; i++)
		for (int j = 0; j <= 9; j++) {
			if (rand() % 2) field[i][j].set_state(CellState::Alive);
			else field[i][j].set_state(CellState::Dead);
		}
	(*this).show();
		}

void Game::set(int i, int j) {
	field[i][j].set_state(CellState::Alive);
	(*this).show();
}

void Game::erase(int i, int j) {
	field[i][j].set_state(CellState::Dead);
	(*this).show();
}

void Game::reset() {
	steps_counter = 0;
	for (int i = 0; i <= 9; i++)
		for (int j = 0; j <= 9; j++)
			field[i][j].set_state(CellState::Dead);
	(*this).show();
}

void Game::step() {
	for (int i = 0; i <= 9; i++)
		for (int j = 0; j <= 9; j++) 
			prev_field[i][j] = field[i][j];

	for (int i = 0; i <= 9; i++)
		for (int j = 0; j <= 9; j++)
			field[i][j].set_state(state_predictor(i, j));
	
	steps_counter++;
	(*this).show();
}

void Game::step(int n) {
	for (int i = 0; i < n; i++) (*this).step();
}

void Game::load(std::string f_name) {
	try {
		std::ifstream in(f_name);
		if (!in.is_open()) throw std::runtime_error("Cannot open the file");
		for (int i = 0; i <= 9; i++) {
			for (int j = 0; j <= 9; j++)
			{
				if (in.eof()) throw std::runtime_error("Invalid input file");
				char ch;
				in >> ch;
				if (ch == '1') field[i][j].set_state(CellState::Alive);
				else if (ch == '0') field[i][j].set_state(CellState::Dead);
				else throw std::runtime_error("Invalid input file");
			}
		}
    }
	catch (std::runtime_error &e) {
		std::cerr << e.what() << std::endl;
	}
}

void Game::save(const std::string f_name) {
	try {
		std::ofstream out(f_name);
		if (!out.is_open()) throw std::runtime_error("Cannot open the file for writing");
		for (int i = 0; i <= 9; i++) {
			for (int j = 0; j <= 9; j++)
			{
				char ch;
				if (field[i][j].get_state() == CellState::Alive) ch = '1';
				else ch = '0';
				out << ch;
			}
		}
	}
	catch (std::runtime_error &e) {
		std::cerr << e.what() << std::endl;
	}
	std::cout << "The pattern has been succesfully written into" << f_name << std::endl;
}

void Game::help() {
	std::cout << "================================= Welcome to the Game Of Life! ==============================" << std::endl;
	std::cout << "Here's the list of commands:" << std::endl;
	std::cout << "show - shows the current field. !!!Start the game with this command!!!" << std::endl;
	std::cout << "load [filename] - loads the field from the corresponding file" << std::endl;
	std::cout << "set [i] [j] - sets the cell with coordinates (i,j) as alive" << std::endl;
	std::cout << "erase [i] [j] - sets the cell with coordinates (i,j) as empty" << std::endl;
	std::cout << "reset	- sets all the cells as empty" << std::endl;
	std::cout << "random_set - sets the alive cells randomly" << std::endl;
	std::cout << "step - runs the game for 1 step" << std::endl;
	std::cout << "step [n] - runs the game for n steps" << std::endl;
	std::cout << "save [filename] - saves the current pattern of a game to the file game.gol" << std::endl;
	std::cout << "help - shows the list of the commads and rules" << std::endl;
	std::cout << "exit - closes the game" << std::endl;
	std::cout << "The rules are simple: while the living cell has 2-3 neigbours, it keeps staying alive. " << std::endl;
	std::cout << "Otherwise, it dies. Instead, an empty cell becomes alive when it gets 3 living neighours. " << std::endl;
	std::cout << "Your aim is to create pattern of cells, in which cells will be living as long as possible." << std::endl;
	std::cout << "Good luck!" << std::endl << std::endl;
	std::cout << "Press Enter to quit.." << std::endl;
	std::cin.get();
	system("cls");
}

int Game::exec_command() {
	std::string msg;
	while (std::getline(std::cin, msg)) {
		try {
			if (msg == "exit") {
				return 0;
			}
			if (msg == "show") {
				(*this).show();
				continue;
			}
			if (msg == "step") {
				(*this).step();
				continue;
			}
			if (msg == "reset") {
				(*this).reset();
				continue;
			}
			if (msg == "random_set") {
				(*this).random_set();
				continue;
			}
			if (msg == "help") {
				(*this).help();
				continue;
			}
			int k = 0;
			while (msg[k] != ' ') {
				if (k == msg.size() - 1) throw std::runtime_error("No argument for the command");
				k++;
			}
			std::string cmd;
			for (int i = 0; i < k; i++) {
				cmd += msg[i];
			}
			if (cmd == "step") {
				std::string num;
				for (int i = k + 1; i < msg.size(); i++) {
					num += msg[i];
				}
				(*this).step(stoi(num));
				continue;
			}
			if (cmd == "set") {
				std::string si, sj;
				for (int i = k + 1; ((i < msg.size()) && (msg[i] != ' ')); i++) {
					si += msg[i];
					k = i;
				}
				for (int i = k + 1; i < msg.size(); i++) {
					sj += msg[i];
				}
				int i = stoi(si);
				int j = stoi(sj);
				if (i < 0 || i > 9) throw std::runtime_error("Bad i coordinate in the argument");
				if (j < 0 || j > 9) throw std::runtime_error("Bad j coordinate in the argument");
				(*this).set(i, j);
				continue;
			}
			if (cmd == "erase") {
				std::string si, sj;
				for (int i = k + 1; ((i < msg.size()) && (msg[i] != ' ')); i++) {
					si += msg[i];
					k = i;
				}
				for (int i = k + 1; i < msg.size(); i++) {
					sj += msg[i];
				}
				int i = stoi(si);
				int j = stoi(sj);
				if (i < 0 || i > 9) throw std::runtime_error("Bad i coordinate in the argument");
				if (j < 0 || j > 9) throw std::runtime_error("Bad j coordinate in the argument");
				(*this).erase(i, j);
				continue;
			}
			if (cmd == "load") {
				std::string name;
				for (int i = k + 1; i < msg.size(); i++) {
					name += msg[i];
				}
				(*this).load(name);
				continue;
			}
			if (cmd == "save") {
				std::string name;
				for (int i = k + 1; i < msg.size(); i++) {
					name += msg[i];
				}
				(*this).save(name);
				continue;
			}
			throw std::runtime_error("Unknown command");
		}
		catch (std::runtime_error &e) {
			std::cerr << e.what() << std::endl;
		}
	}
	return -1;
}